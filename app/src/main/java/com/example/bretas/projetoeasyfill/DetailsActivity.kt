package com.example.bretas.projetoeasyfill

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_cadastro.*
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.posto_item_lista.*

class DetailsActivity : AppCompatActivity() {

    companion object {
        public const val POSTO = "Posto"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val posto: Posto? = intent.getSerializableExtra(POSTO) as Posto?

        if(posto != null){
            carregaDados(posto)
        }

        imgMaps.setOnClickListener{
            mostraNoMapa()
        }



    }

    private fun carregaDados(posto: Posto) {
        tvNomePosto.text = posto.nomePosto
        tvEndPosto.text  = posto.endereco
        tvPrecoGasolina.text = posto.precoGasolina
        tvPrecoEtanol.text = posto.precoEtanol
        tvPrecoDiesel.text = posto.precoDiesel
        //edtCidade.setText(posto.cidade)
        //edtEstado.setText(posto.estado)

    }

    private fun mostraNoMapa() {
        val mostrarNoMapa = Intent(Intent.ACTION_VIEW)
        mostrarNoMapa.data = Uri.parse("geo:0,0?q=$(tvEndPosto.text}")

        if (mostrarNoMapa.resolveActivity(packageManager) != null) {
            startActivity(mostrarNoMapa)
        } else {
            Toast.makeText(this, "Impossível acessar endereço", Toast.LENGTH_SHORT).show()
        }
    }
}
