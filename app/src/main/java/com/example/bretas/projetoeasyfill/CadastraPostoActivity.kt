package com.example.bretas.projetoeasyfill

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_cadastro.*

class CadastraPostoActivity : AppCompatActivity() {

    companion object {
        public const val POSTO = "Posto"
        public const val REQUEST_CAMERA = 10
        public const val REQUEST_PERMISSION = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro)

        btnFoto.setOnClickListener(){
           tiraFoto()
        }

    }

    private fun tiraFoto() {
        val tirarFoto = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        tirarFoto.data = Uri.parse("geo:0,0?q=$(tvEndPosto.text}")

        if (tirarFoto.resolveActivity(packageManager) != null) {
            startActivityForResult(tirarFoto, REQUEST_CAMERA)
        } else {
            Toast.makeText(this, "Impossível tirar foto", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cadastro, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
           R.id.menuSalvar -> salvaPosto()
        }

        return super.onOptionsItemSelected(item)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        requestPermissions(arrayOf(android.Manifest.permission.CAMERA), REQUEST_PERMISSION)
        if(requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK){
            val bitmap = data?.extras?.get("dats") as Bitmap
            imgFoto.setImageBitmap(bitmap)

        }
    }

    private fun salvaPosto() {
        val posto = Posto(edtNome.text.toString(),
                          edtCidade.text.toString(),
                          edtEndereco.text.toString(),
                          edtEstado.text.toString(),
                          edtPrDiesel.text.toString(),
                          edtPrEtanol.text.toString(),
                          edtPrGasolina.text.toString())

        //Toast.makeText(this, posto.toString(), Toast.LENGTH_LONG).show()

        val abreLista = Intent(this, ListaPostoActivity::class.java)
        abreLista.putExtra(POSTO, posto)

        setResult(Activity.RESULT_OK, abreLista)
        finish()
    }

}
