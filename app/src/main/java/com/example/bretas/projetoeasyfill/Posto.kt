package com.example.bretas.projetoeasyfill

import java.io.Serializable

data class Posto(val nomePosto: String,
                 val endereco: String,
                 val cidade: String,
                 val estado: String,
                 val precoGasolina: String,
                 val precoEtanol: String,
                 val precoDiesel: String): Serializable