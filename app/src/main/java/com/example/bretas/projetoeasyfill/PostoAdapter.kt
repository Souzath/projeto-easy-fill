package com.example.bretas.projetoeasyfill

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.posto_item_lista.view.*
import kotlin.coroutines.experimental.coroutineContext

class PostoAdapter(val context: Context, val postos: List<Posto>)
    : RecyclerView.Adapter<PostoAdapter.ViewHolder>() {

    var clique: ((posto:Posto) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.posto_item_lista, parent, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return postos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(context, postos[position], clique)
    }

    fun configuraClique(clique: ((posto:Posto)->Unit)){

        this.clique = clique
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(context: Context, posto: Posto, clique: ((posto:Posto)->Unit)?) {
            itemView.tvNomePosto.text = posto.nomePosto
            itemView.tvEndPosto.text = posto.endereco
            itemView.tvPrecoGasolina.text = posto.precoGasolina
            itemView.tvPrecoEtanol.text = posto.precoEtanol
            itemView.tvPrecoDiesel.text = posto.precoDiesel

            if(clique != null) {
                itemView.setOnClickListener() {
                    clique.invoke(posto)
                    /* val detalhesPosto = Intent(context, DetailsActivity::class.java)
                detalhesPosto.putExtra(CadastraPostoActivity.POSTO, posto)
                context.startActivity(detalhesPosto)*/
                }
            }
        }

    }
}