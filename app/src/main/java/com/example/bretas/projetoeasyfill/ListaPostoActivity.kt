package com.example.bretas.projetoeasyfill

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_lista_posto.*

class ListaPostoActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CADASTRO = 1
        private const val LISTA = "ListaPostos"

    }

    var listaPostos : MutableList<Posto> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_posto)



        btnAddPosto.setOnClickListener(){
            val cadastraPosto = Intent(this, CadastraPostoActivity::class.java)
            startActivityForResult(cadastraPosto, REQUEST_CADASTRO)
        }
    }

    override fun onResume() {
        super.onResume()
        carregaLista()
    }



    fun carregaLista() {
        val adapter = PostoAdapter(this, listaPostos)

        adapter.configuraClique {
            val detalhesPosto = Intent(this, DetailsActivity::class.java)
                detalhesPosto.putExtra(DetailsActivity.POSTO, it)
                this.startActivity(detalhesPosto)
        }

        val layoutManager = LinearLayoutManager(this)

        rvPostos.adapter = adapter
        rvPostos.layoutManager = layoutManager
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_CADASTRO && resultCode == Activity.RESULT_OK){
            val novoPosto1: Posto? = data?.getSerializableExtra(CadastraPostoActivity.POSTO) as Posto

            if (novoPosto1 != null) {
                listaPostos.add(novoPosto1)
            }

        }

    }


    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)

        outState?.putSerializable(LISTA, listaPostos as ArrayList<String>)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        if(savedInstanceState != null){
            listaPostos = savedInstanceState.getSerializable(LISTA) as MutableList<Posto>
        }
    }

}
